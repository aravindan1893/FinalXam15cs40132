package big_web.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


//import BTAC.SASample.filpkart.webMethods.clearTripMethod;
import big_web.methods.go_method;
import btac.sa.lib.BrowserActions;
import btac.sa.lib.MouseAndKeyboard;
import btac.sa.lib.SelectBrowser;
import btac.sa.lib.WebCommonMethods;

public class go_test extends SelectBrowser {
	WebCommonMethods general;
	go_method grofers;
	BrowserActions ba;
	MouseAndKeyboard mk;

	String appserver;
	
	@BeforeMethod
    public void openTheBrowser() throws Exception 
    {
    	WebDriver d = getBrowser();
    	grofers = PageFactory.initElements(d, go_method.class);
	    general = PageFactory.initElements(d, WebCommonMethods.class);// initiating the driver and the .class file (the pageObject script)	    
	    ba = PageFactory.initElements(d, BrowserActions.class);// initiating the driver and the .class file (the pageObject script)	    
	    mk = PageFactory.initElements(d, MouseAndKeyboard.class);// initiating the driver and the .class file (the pageObject script)	    

	    BrowserActions.openURLBasedOnDbDomain();
    } 
	
	@Test(priority=1, groups={"loginlogout"})
	public void gibiboFirstMethod() throws Exception
	{	
		System.out.println("************************clear trip started***************************");
		grofers.firstMethod();
		System.out.println("++++++++++++++++++++++++clear Search success+++++++++++++++++++++++++++");
	}
	
	@AfterMethod(alwaysRun=true)
    public void catchExceptions(ITestResult result) throws Exception 
    {    
    	String methodname = result.getName();
        if(!result.isSuccess()){            
        	WebCommonMethods.screenshot(methodname);
        }
        BrowserActions.quit(); // Calling function close to quit browser instance
    }
	
}
