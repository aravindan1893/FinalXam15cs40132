package big_web.methods;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import btac.sa.lib.SelectBrowser;

public class go_method extends SelectBrowser {

	public static WebDriver driver;
	public go_method(WebDriver driver){
		this.driver=driver;
	}
	static Workbook wrk1;
	static Sheet sheet1;
	static Cell colRow;
    @FindBy(xpath = ".//*[@id='app']/div/div[2]/div[2]/header/div[2]/div[2]/div/div/div/div[2]/div/div[2]/div[2]/div[3]/div")
	public static WebElement Bangalore;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[2]/header/div[2]/div/div[1]/input")
	public static WebElement SearchBox;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[2]/header/div[2]/div/div[1]/button")
	public static WebElement SearchButton;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[1]/nav/ul/li[2]/a/span")
	public static WebElement showingresultapple;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[1]/div/div[4]/div")
	public static WebElement two2showingresultapple;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[2]/div/div[4]/div")
	public static WebElement three3showingresultapple;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[3]/div/div[4]/div")
	public static WebElement four4showingresultapple;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[4]/div/div[4]/div")
	public static WebElement five5showingresultapple;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[5]/div/div[4]/div")
	public static WebElement six6howingresultapple;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[6]/div/div[4]/div")
	public static WebElement seven7showingresultapple;
	
	@FindBy(xpath = ".//*[@id='app']/div/div[2]/div[4]/div/div/div[2]/div[1]/div/div/div/div[2]/div/div[2]/div[1]/a[7]/div/div[4]/div")
	public static WebElement eight8showingresultapple;
	
	public static void firstMethod() throws Exception
	{
		Bangalore.click();
		Thread.sleep(3000);
		SearchBox.sendKeys("Apple");
		Thread.sleep(3000);
		SearchButton.click();
		Thread.sleep(3000);
		
		String one1= showingresultapple.getText();
		//Assert.assertEquals(one1,"Apple");
		Thread.sleep(2000);
		System.out.println(one1+"-----------------------Frist Value Asserted For Apple-------------------------------------");
		
		String two2= two2showingresultapple.getText();
		Assert.assertEquals("Washington Apple - Imported",two2);
		Thread.sleep(2000);
		System.out.println(two2+"---------------Second value Asserted For Apple---------------------------------------------");
		
		String three3= three3showingresultapple.getText();
		Assert.assertEquals("Indian Apple",three3);
		Thread.sleep(2000);
		System.out.println(two2+"---------------Third value Asserted For Apple---------------------------------------------");
		
		String four4= four4showingresultapple.getText();
		Assert.assertEquals("Royal Gala Apple - Imported",four4);
		Thread.sleep(2000);
		System.out.println(four4+"---------------foruth value Asserted For Apple---------------------------------------------");
		
		String five5= five5showingresultapple.getText();
		Assert.assertEquals("Green Apple - Imported",five5);
		Thread.sleep(2000);
		System.out.println(five5+"---------------fifth value Asserted For Apple---------------------------------------------");
		
		String six6= six6howingresultapple.getText();
		Assert.assertEquals("Custard Apple",six6);
		Thread.sleep(2000);
		System.out.println(six6+"---------------sixth value Asserted For Apple---------------------------------------------");
		
		String seven7= seven7showingresultapple.getText();
		Assert.assertEquals("Indian Apple-Papaya Combo",seven7);
		Thread.sleep(2000);
		System.out.println(seven7+"---------------seventh value Asserted For Apple---------------------------------------------");
		
		String eigth8= eight8showingresultapple.getText();
		Assert.assertEquals("Real Fruit Power Apple Juice",eigth8);
		Thread.sleep(2000);
		System.out.println(eigth8+"---------------eight value Asserted For Apple---------------------------------------------");
		
	}

}
